package hu.szlgbp.konyvtar;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class SettingsFragment extends PreferenceFragment {

    //Variables
    public static Preference tanker_text;
    public static Preference suli_text;
    public static Preference status_text;
    public static Preference weblap_text;
    public static Preference key_text;
    public static String checkState_key = "";
    static RequestQueue requestQueue;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        //Variables
        Preference scan_btn = findPreference("btn_scan_setting");
        tanker_text = findPreference("tanker_text");
        suli_text = findPreference("suli_text");
        weblap_text = findPreference("weblap_text");
        status_text = findPreference("status_text");
        key_text = findPreference("key_text");

        //Scan
        scan_btn.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(){
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(getActivity(), ScanCodeActivity.class);
                String from = "settings";
                intent.putExtra("from", from);
                startActivity(intent);
                return true;
            }
        });

        //Request context
        requestQueue = Volley.newRequestQueue(getActivity());

        if (MainActivity.baseUrl != null) {
            //region setstatus
            StringRequest statusrequest = new StringRequest
                    (Request.Method.POST, MainActivity.baseUrl + MainActivity.checkstateUrl, new Response.Listener<String>() {

                        @RequiresApi(api = Build.VERSION_CODES.O)
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject o = null;
                                o = new JSONObject(response);
                                if (o.getBoolean("success")) {
                                    //Tanker, Suli, Status
                                    weblap_text.setSummary(MainActivity.baseUrl);
                                    tanker_text.setSummary(o.getString("tanker"));
                                    suli_text.setSummary(o.getString("suli"));
                                    status_text.setSummary("Csatlakozva");
                                    StringBuilder sb = new StringBuilder();
                                    try {
                                        MessageDigest digest = MessageDigest.getInstance("SHA-256");
                                        byte[] hash = digest.digest(MainActivity.key.getBytes());
                                        for (byte x : hash) {
                                            String str = Integer.toHexString(Byte.toUnsignedInt(x));
                                            if (str.length() < 2) {
                                                sb.append('0');
                                            }
                                            sb.append(str);
                                        }
                                    } catch (NoSuchAlgorithmException e) {
                                        e.printStackTrace();
                                    }
                                    SettingsFragment.key_text.setSummary(sb.toString());
                                } else {
                                    MainActivity.error_handler(o.getString("message"), getActivity());
                                }
                            } catch (JSONException e) {
                                MainActivity.error_handler(e.toString(), getActivity());
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            MainActivity.error_handler(error.toString(), getActivity());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("appkey", MainActivity.key);
                    return params;
                }
            };
            requestQueue.add(statusrequest);
            //endregion
        } else {
            Toast.makeText(getActivity(), "Regisztráció szükséges", Toast.LENGTH_SHORT).show();
        }
    }
}
