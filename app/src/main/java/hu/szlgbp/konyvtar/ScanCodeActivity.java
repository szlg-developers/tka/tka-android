package hu.szlgbp.konyvtar;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.Result;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanCodeActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    //variables
    static RequestQueue requestQueue;
    ZXingScannerView ScannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ScannerView = new ZXingScannerView(this);
        setContentView(ScannerView);
    }

    @Override
    public void handleResult(final Result result) {
        String from = getIntent().getStringExtra("from");
        requestQueue = Volley.newRequestQueue(this);

        if (from.equals("main")) {
            MainActivity.book_added = true;
            MainActivity.text.add(result.getText());
            MainActivity.resultTextView.setText("");
            for (String t : MainActivity.text) {
                MainActivity.resultTextView.append(t);
                MainActivity.resultTextView.append("\n");
            }
        }
        else if (from.equals("settings")) {
            //region get realkey
            StringRequest getkey = new StringRequest
                    (Request.Method.POST, result.toString(), new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject o = null;
                                o = new JSONObject(response);
                                if (o.getBoolean("success")) {
                                    //save url
                                    String[] weblap = result.toString().split("/");
                                    String weblap_text = "";
                                    for (int i = 0; i < weblap.length - 4; i++) {
                                        weblap_text += weblap[i] + "/";
                                    }
                                    MainActivity.baseUrl = weblap_text;
                                    MainActivity.key = o.getString("key");

                                    SharedPreferences.Editor editor = MainActivity.urlPref.edit();
                                    editor.putString("url", weblap_text);
                                    editor.putString("key", o.getString("key"));
                                    editor.apply();
                                    editor.commit();

                                    //region setstatus
                                    StringRequest statusrequest = new StringRequest
                                            (Request.Method.POST, MainActivity.baseUrl + MainActivity.checkstateUrl, new Response.Listener<String>() {

                                                @RequiresApi(api = Build.VERSION_CODES.O)
                                                @Override
                                                public void onResponse(String response) {
                                                    try {
                                                        JSONObject o = null;
                                                        o = new JSONObject(response);
                                                        if (o.getBoolean("success")) {
                                                            //Tanker, Suli, Status
                                                            SettingsFragment.weblap_text.setSummary(MainActivity.baseUrl);
                                                            SettingsFragment.tanker_text.setSummary(o.getString("tanker"));
                                                            SettingsFragment.suli_text.setSummary(o.getString("suli"));
                                                            SettingsFragment.status_text.setSummary("Csatlakozva");
                                                            StringBuilder sb = new StringBuilder();
                                                            try {
                                                                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                                                                byte[] hash = digest.digest(MainActivity.key.getBytes());
                                                                for (byte x : hash) {
                                                                    String str = Integer.toHexString(Byte.toUnsignedInt(x));
                                                                    if (str.length() < 2) {
                                                                        sb.append('0');
                                                                    }
                                                                    sb.append(str);
                                                                }
                                                            } catch (NoSuchAlgorithmException e) {
                                                                e.printStackTrace();
                                                            }
                                                            SettingsFragment.key_text.setSummary(sb.toString());

                                                            //region set_mainactivity_actions
                                                            //data lists
                                                            final ArrayList<String> osztályok_list = new ArrayList<>();
                                                            final ArrayList<String> osztályok_id_list = new ArrayList<>();
                                                            final ArrayList<String> diákok_list = new ArrayList<>();
                                                            final ArrayList<String> diák_id_list = new ArrayList<>();

                                                            //region classes
                                                            StringRequest classrequest = new StringRequest
                                                                    (Request.Method.POST, MainActivity.baseUrl + MainActivity.classUrl, new Response.Listener<String>() {
                                                                        @Override
                                                                        public void onResponse(String response) {
                                                                            try {
                                                                                JSONObject o = null;
                                                                                o = new JSONObject(response);
                                                                                if (o.getBoolean("success")) {
                                                                                    JSONArray oszt = o.getJSONArray("osztály");
                                                                                    JSONArray oszt_id = o.getJSONArray("osztály_id");

                                                                                    for (int i = 0; i < oszt.length(); i++) {
                                                                                        osztályok_list.add(oszt.getString(i));
                                                                                        osztályok_id_list.add(oszt_id.getString(i));
                                                                                    }

                                                                                    ArrayAdapter<String> osztályadapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item, osztályok_list);
                                                                                    osztályadapter.setDropDownViewResource(R.layout.spinner_item);
                                                                                    MainActivity.osztályok.setAdapter(osztályadapter);
                                                                                } else {
                                                                                    MainActivity.error_handler(o.getString("message"), getApplicationContext());
                                                                                }
                                                                            } catch (JSONException e) {
                                                                                MainActivity.error_handler(e.toString(), getApplicationContext());
                                                                            }
                                                                        }
                                                                    }, new Response.ErrorListener() {

                                                                        @Override
                                                                        public void onErrorResponse(VolleyError error) {
                                                                            MainActivity.error_handler(error.toString(), getApplicationContext());
                                                                        }
                                                                    }) {
                                                                @Override
                                                                protected Map<String, String> getParams() throws AuthFailureError {
                                                                    Map<String, String> params = new HashMap<>();
                                                                    params.put("appkey", MainActivity.key);
                                                                    return params;
                                                                }
                                                            };
                                                            requestQueue.add(classrequest);
                                                            //endregion

                                                            MainActivity.osztályok.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                                @Override
                                                                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                                                                    //region students
                                                                    StringRequest studentrequest = new StringRequest
                                                                            (Request.Method.POST, MainActivity.baseUrl + MainActivity.studentUrl, new Response.Listener<String>() {
                                                                                @Override
                                                                                public void onResponse(String response) {
                                                                                    try {
                                                                                        JSONObject o = null;
                                                                                        o = new JSONObject(response);
                                                                                        if (o.getBoolean("success")) {
                                                                                            JSONArray diá = o.getJSONArray("diak");
                                                                                            JSONArray diá_id = o.getJSONArray("diak_id");

                                                                                            diákok_list.clear();
                                                                                            diák_id_list.clear();

                                                                                            for (int i = 0; i < diá.length(); i++) {
                                                                                                diákok_list.add(diá.getString(i));
                                                                                                diák_id_list.add(diá_id.getString(i));
                                                                                            }

                                                                                            ArrayAdapter<String> diákadapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item, diákok_list);
                                                                                            diákadapter.setDropDownViewResource(R.layout.spinner_item);
                                                                                            MainActivity.diákok.setAdapter(diákadapter);
                                                                                        } else {
                                                                                            MainActivity.error_handler(o.getString("message"), getApplicationContext());
                                                                                        }
                                                                                    } catch (JSONException e) {
                                                                                        MainActivity.error_handler(e.toString(), getApplicationContext());
                                                                                    }
                                                                                }
                                                                            }, new Response.ErrorListener() {

                                                                                @Override
                                                                                public void onErrorResponse(VolleyError error) {
                                                                                    MainActivity.error_handler(error.toString(), getApplicationContext());
                                                                                }
                                                                            }) {
                                                                        @Override
                                                                        protected Map<String, String> getParams(){
                                                                            Map<String, String> params = new HashMap<>();
                                                                            String osztály_név = MainActivity.osztályok.getSelectedItem().toString();
                                                                            int osztály_id = osztályok_list.indexOf(osztály_név);
                                                                            params.put("class_id", String.valueOf(osztályok_id_list.get(osztály_id)));
                                                                            params.put("appkey", MainActivity.key);
                                                                            return params;
                                                                        }
                                                                    };
                                                                    requestQueue.add(studentrequest);
                                                                    //endregion
                                                                }
                                                                @Override
                                                                public void onNothingSelected(AdapterView<?> adapterView) {

                                                                }
                                                            });

                                                            //insert
                                                            MainActivity.insert_btn.setOnClickListener(new View.OnClickListener() {
                                                                @Override
                                                                public void onClick(View view) {
                                                                    //region insert
                                                                    StringRequest insertrequest = new StringRequest
                                                                            (Request.Method.POST, MainActivity.baseUrl + MainActivity.insertUrl, new Response.Listener<String>() {
                                                                                @Override
                                                                                public void onResponse(String response) {
                                                                                    try {
                                                                                        JSONObject o = null;
                                                                                        o = new JSONObject(response);
                                                                                        if (o.getBoolean("success")) {
                                                                                            Toast.makeText(getApplicationContext(), "Sikeresen feltöltve " + o.getString("count") + " könyv", Toast.LENGTH_SHORT).show();
                                                                                            MainActivity.text.clear();
                                                                                            MainActivity.resultTextView.setText("");
                                                                                        } else {
                                                                                            MainActivity.error_handler(o.getString("message"), getApplicationContext());
                                                                                        }
                                                                                    } catch (JSONException e) {
                                                                                        MainActivity.error_handler(e.toString(), getApplicationContext());
                                                                                    }
                                                                                }
                                                                            }, new Response.ErrorListener() {

                                                                                @Override
                                                                                public void onErrorResponse(VolleyError error) {
                                                                                    if (MainActivity.text.size() == 0) {
                                                                                        MainActivity.error_handler("Nincs beolvasott könyv.", getApplicationContext());
                                                                                    } else {
                                                                                        MainActivity.error_handler(error.toString(), getApplicationContext());
                                                                                    }
                                                                                }
                                                                            }) {
                                                                        @Override
                                                                        protected Map<String, String> getParams() throws AuthFailureError {
                                                                            Map<String, String> params  = new HashMap<>();
                                                                            String s = "";
                                                                            s += "[";
                                                                            for (String t: MainActivity.text) {
                                                                                s += "\"";
                                                                                s += t;
                                                                                s += "\", ";
                                                                            }
                                                                            s = s.substring(0, s.length() - 2);
                                                                            s += "]";
                                                                            params.put("book_codes", s);
                                                                            String diák_név = MainActivity.diákok.getSelectedItem().toString();
                                                                            int diák_id = diákok_list.indexOf(diák_név);
                                                                            params.put("student_id", String.valueOf(diák_id_list.get(diák_id)));
                                                                            params.put("appkey", MainActivity.key);
                                                                            return params;
                                                                        }
                                                                    };
                                                                    requestQueue.add(insertrequest);
                                                                    //endregion
                                                                }
                                                            });
                                                            //endregion
                                                        } else {
                                                            MainActivity.error_handler(o.getString("message"), getApplicationContext());
                                                        }
                                                    } catch (JSONException e) {
                                                        MainActivity.error_handler(e.toString(), getApplicationContext());
                                                    }
                                                }
                                            }, new Response.ErrorListener() {

                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    MainActivity.error_handler(error.toString(), getApplicationContext());
                                                }
                                            }) {
                                        @Override
                                        protected Map<String, String> getParams() throws AuthFailureError {
                                            Map<String, String> params = new HashMap<>();
                                            params.put("appkey", MainActivity.key);
                                            return params;
                                        }
                                    };
                                    requestQueue.add(statusrequest);
                                    //endregion
                                } else {
                                    MainActivity.error_handler(o.getString("message"), ScanCodeActivity.this);
                                }
                            } catch (JSONException e) {
                                MainActivity.error_handler(e.toString(), ScanCodeActivity.this);
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                        MainActivity.error_handler(error.toString(), ScanCodeActivity.this);
                        }
                    });
            requestQueue.add(getkey);
            //endregion
        }
        onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ScannerView.stopCamera();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        ScannerView.setResultHandler(this);
        ScannerView.startCamera();
    }
}