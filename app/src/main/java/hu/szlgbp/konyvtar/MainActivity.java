package hu.szlgbp.konyvtar;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.security.keystore.WrappedKeyEntry;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    //variables
    static RequestQueue requestQueue;
    //activity items
    public static TextView resultTextView;
    public static Spinner osztályok;
    public static Spinner diákok;
    public static Button insert_btn;
    Button scan_btn;
    Button del_btn;
    public static ArrayList<String> text = new ArrayList<>();
    public static boolean book_added = false;
    public static SharedPreferences urlPref;

    //urls
    public static String key;
    public static String baseUrl = null;
    public static String checkstateUrl;
    public static String insertUrl;
    public static String classUrl;
    public static String studentUrl;
    public static String bookUrl;
    public static String getkeyUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //region init variables
        //activity items
        osztályok = findViewById(R.id.osztáyok);
        diákok = findViewById(R.id.diákok);
        resultTextView = findViewById(R.id.result_text);
        scan_btn = findViewById(R.id.btn_scan);
        del_btn = findViewById(R.id.bnt_del);
        insert_btn = findViewById(R.id.btn_insert);
        urlPref = getSharedPreferences("urlPref", Context.MODE_PRIVATE);

        //urls
        checkstateUrl = "/web/backend/android/checkState.php";
        insertUrl = "/web/backend/android/insertStudent.php";
        classUrl = "/web/backend/android/showClasses.php";
        studentUrl = "/web/backend/android/showStudents.php";
        bookUrl = "/web/backend/android/getSchool.php";
        getkeyUrl = "/web/backend/android/registKey.php";
        //endregion


        //region get permissions
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.CAMERA
        };

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

        //endregion


        //region get data and create file
        baseUrl = urlPref.getString("url", null);
        key = urlPref.getString("key", null);

        File dir = getFilesDir();
        File file = new File(dir, "log.txt");
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                Toast.makeText(this, "Couldn't create log file", Toast.LENGTH_SHORT).show();
            }
        }
        //endregion

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        //click events
        //Scan
        scan_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ScanCodeActivity.class);
                intent.putExtra("from", "main");
                startActivity(intent);
            }
        });

        //Del content
        del_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (text.size() > 0)
                {
                    text.remove(text.size() - 1);
                    resultTextView.setText("");
                    for (String t: MainActivity.text)
                    {
                        MainActivity.resultTextView.append(t);
                        MainActivity.resultTextView.append("\n");
                    }
                }
            }
        });

        if (baseUrl != null) {
            //data lists
            final ArrayList<String> osztályok_list = new ArrayList<>();
            final ArrayList<String> osztályok_id_list = new ArrayList<>();
            final ArrayList<String> diákok_list = new ArrayList<>();
            final ArrayList<String> diák_id_list = new ArrayList<>();

            //region classes
            StringRequest classrequest = new StringRequest
                    (Request.Method.POST, baseUrl + classUrl, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject o = null;
                                o = new JSONObject(response);
                                if (o.getBoolean("success")) {
                                    JSONArray oszt = o.getJSONArray("osztály");
                                    JSONArray oszt_id = o.getJSONArray("osztály_id");

                                    for (int i = 0; i < oszt.length(); i++) {
                                        osztályok_list.add(oszt.getString(i));
                                        osztályok_id_list.add(oszt_id.getString(i));
                                    }

                                    ArrayAdapter<String> osztályadapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item, osztályok_list);
                                    osztályadapter.setDropDownViewResource(R.layout.spinner_item);
                                    osztályok.setAdapter(osztályadapter);
                                } else {
                                    error_handler(o.getString("message"), MainActivity.this);
                                }
                            } catch (JSONException e) {
                                error_handler(e.toString(), MainActivity.this);
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error_handler(error.toString(), MainActivity.this);
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("appkey", key);
                    return params;
                }
            };
            requestQueue.add(classrequest);
            //endregion

            osztályok.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    //region students
                    StringRequest studentrequest = new StringRequest
                            (Request.Method.POST, baseUrl + studentUrl, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject o = null;
                                        o = new JSONObject(response);
                                        if (o.getBoolean("success")) {
                                            JSONArray diá = o.getJSONArray("diak");
                                            JSONArray diá_id = o.getJSONArray("diak_id");

                                            diákok_list.clear();
                                            diák_id_list.clear();

                                            for (int i = 0; i < diá.length(); i++) {
                                                diákok_list.add(diá.getString(i));
                                                diák_id_list.add(diá_id.getString(i));
                                            }

                                            ArrayAdapter<String> diákadapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item, diákok_list);
                                            diákadapter.setDropDownViewResource(R.layout.spinner_item);
                                            diákok.setAdapter(diákadapter);
                                        } else {
                                            error_handler(o.getString("message"), MainActivity.this);
                                        }
                                    } catch (JSONException e) {
                                        error_handler(e.toString(), MainActivity.this);
                                    }
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    error_handler(error.toString(), MainActivity.this);
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams(){
                            Map<String, String> params = new HashMap<>();
                            String osztály_név = osztályok.getSelectedItem().toString();
                            int osztály_id = osztályok_list.indexOf(osztály_név);
                            params.put("class_id", String.valueOf(osztályok_id_list.get(osztály_id)));
                            params.put("appkey", key);
                            return params;
                        }
                    };
                    requestQueue.add(studentrequest);
                    //endregion
                }
                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            //insert
            insert_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //region insert
                    StringRequest insertrequest = new StringRequest
                            (Request.Method.POST, baseUrl + insertUrl, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject o = null;
                                        o = new JSONObject(response);
                                        if (o.getBoolean("success")) {
                                            Toast.makeText(getApplicationContext(), "Sikeresen feltöltve " + o.getString("count") + " könyv", Toast.LENGTH_SHORT).show();
                                            text.clear();
                                            resultTextView.setText("");
                                        } else {
                                            error_handler(o.getString("message"), MainActivity.this);
                                        }
                                    } catch (JSONException e) {
                                        error_handler(e.toString(), MainActivity.this);
                                    }
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    if (text.size() == 0) {
                                        Toast.makeText(getApplicationContext(), "Nincs beolvasott könyv", Toast.LENGTH_SHORT).show();
                                    } else {
                                        error_handler(error.toString(), MainActivity.this);
                                    }
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params  = new HashMap<>();
                            String s = "";
                            s += "[";
                            for (String t: text) {
                                s += "\"";
                                s += t;
                                s += "\", ";
                            }
                            s = s.substring(0, s.length() - 2);
                            s += "]";
                            params.put("book_codes", s);
                            String diák_név = diákok.getSelectedItem().toString();
                            int diák_id = diákok_list.indexOf(diák_név);
                            params.put("student_id", String.valueOf(diák_id_list.get(diák_id)));
                            params.put("appkey", key);
                            return params;
                        }
                    };
                    requestQueue.add(insertrequest);
                    //endregion
                }
            });
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        if (baseUrl != null) {
            //region showbook
            if (text.size() > 0 && book_added) {
                book_added = false;
                StringRequest bookrequest = new StringRequest
                        (Request.Method.POST, baseUrl + MainActivity.bookUrl, new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject o = null;
                                    o = new JSONObject(response);
                                    if (o.getBoolean("success")) {
                                        //dialog box
                                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                                        builder1.setMessage(o.getString("title"));
                                        builder1.setCancelable(true);
                                        AlertDialog alert11 = builder1.create();
                                        alert11.show();
                                    } else {
                                        MainActivity.error_handler(o.getString("message"), MainActivity.this);
                                    }
                                } catch (JSONException e) {
                                    MainActivity.error_handler(e.toString(), MainActivity.this);
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                MainActivity.error_handler(error.toString(), MainActivity.this);
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("appkey", key);
                        params.put("code", text.get(text.size() - 1));
                        return params;
                    }
                };
                requestQueue.add(bookrequest);
            }
            //endregion
            } else {
                Toast.makeText(getApplicationContext(), "Regisztráció szükséges", Toast.LENGTH_SHORT).show();
            }
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.settings_menu) {
            Intent intent = new Intent(MainActivity.this, Settings.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static void error_handler(String error, Context context) {
        try {
            //logging
            File file = new File(context.getFilesDir(), "log.txt");
            FileWriter writer = new FileWriter(file);
            Calendar now = Calendar.getInstance();
            String log = now.getTime() + " - " + error + "\n";
            writer.append(log);
            writer.flush();
            writer.close();

            //dialog box
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
            builder1.setMessage(error);
            builder1.setCancelable(true);
            AlertDialog alert11 = builder1.create();
            alert11.show();

        } catch (IOException e) {
            Toast.makeText(context, "Couldn't save error", Toast.LENGTH_SHORT).show();
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}